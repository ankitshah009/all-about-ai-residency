# all-about-ai-residency

AI residency programs information
================================

## Companies and their program links

#### Uber AI Residency - https://eng.uber.com/uber-ai-residency/

#### Facebook AI Residency - https://research.fb.com/programs/facebook-ai-research-residency-program/

#### Google AI residency - https://ai.google/research/join-us/ai-residency/

#### Microsoft AI Residency - https://www.microsoft.com/en-us/research/academic-program/microsoft-ai-residency-program/

#### Clova AI Residency - https://clova.ai/m/en/research/careers.html

#### Open AI Fellow - https://blog.openai.com/openai-fellows/

#### Intel AI Residency - https://databricks.com/jobs/intel-ai-residency

#### PFN AI Residency - https://www.preferred-networks.jp/en/news/residency-program2018-2019tokyo

#### NVIDIA AI Residency - https://research.nvidia.com/research-residency 

## How to prepare

#### Google Brain Residency - https://www.quora.com/How-should-I-prepare-for-the-interview-of-Google-Brain-Residency-program-in-terms-of-number-of-rounds-of-interview-topics-for-which-I-should-prepare-and-a-typical-profile-required-to-increase-the-chance-of-being-selected

#### Google AI residency - https://docs.google.com/document/d/14nNwC1T0oANp4qFYeKupGh4w5lZUDmA9jUMi2YW7Z7k/edit?usp=sharing

